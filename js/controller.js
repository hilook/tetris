/*
 キーボードを入力した時に一番最初に呼び出される処理
 */
document.body.onkeydown = function( e ) {
  // キーに名前をセットする
  if(pause == false){
	var keys = {
    37: 'left',       // utf-8でのキーボード左が３７番目のボタン
    39: 'right',
    40: 'down',
    38: 'rotate',
    16: 'pause',
  };
  }
  else{
	  var keys = {
	  16: 'pause'
	  };
  }

  if ( typeof keys[ e.keyCode ] != 'undefined' ) {
    // セットされたキーの場合はtetris.jsに記述された処理を呼び出す
    keyPress( keys[ e.keyCode ] );
    // 描画処理を行う
    render();
  }
};
