var lineScore = 0;
var COLS = 10, ROWS = 20;  // 盤面のマスの数
var board = [];  // 盤面の状態を保持する変数
var lose;  // 一番うえまで積み重なっちゃったフラグ
var interval;  // ゲームタイマー保持用変数
var current; // 現在操作しているブロック
var currentX, currentY; // 現在操作しているブロックのいち
var pause = false;
// ブロックのパターン・７種類・要素の要素
var shapes = [
  [ 1, 1, 1, 1 ],   // i = [0,1,2,3]
  [ 1, 1, 1, 0,     // i = [0,1,2,4]
    1 ],
  [ 1, 1, 1, 0,     // i = [0,1,2,6]
    0, 0, 1 ],
  [ 1, 1, 0, 0,     // i = [0,1,4,5]
    1, 1 ],
  [ 1, 1, 0, 0,     // i = [0,1,5,6]
    0, 1, 1 ],
  [ 0, 1, 1, 0,
    1, 1 ],
  [ 0, 1, 0, 0,
    1, 1, 1 ]
];
// ブロックの色
var colors = [
  'cyan', 'orange', 'blue', 'yellow', 'red', 'green', 'purple'
  ];
// shapesからランダムにブロックのパターンを出力し、盤面の一番上へセットする
function newShape() {
  var id = Math.floor( Math.random() * shapes.length );
/*ランダムにインデックスを出す。shapes.lengthはshapesの要素数
  Math.random()  0 以上 1 未満の範囲で浮動少数を疑似乱数で生成
  Math.floor()   引数として与えた数以下の最大の整数を返します。 */
  var shape = shapes[ 0 ];  // 確定したパターンを操作ブロックへセットする
  current = [];                // 配列の宣言
  for ( var y = 0; y < 4; ++y ) {     //4X4マスを基にブロックを生成
    current[ y ] = [];        //変数宣言時にvarを省略できるが、省略すると必ずグローバル変数になる。
    for ( var x = 0; x < 4; ++x ) {
      var i = 4 * y + x;        //0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
      if ( typeof shape[ i ] != 'undefined' && shape[ i ] ) {
    	  //typeof演算子：オペランドのデータ型を示す文字列を返します。undefined 未定義
        current[ y ][ x ] = id + 1;    //  ４ｘ４の各マスに色をつけるか、どんな色をつけるかを指定
      }
      else {
        current[ y ][ x ] = 0;
      }
    }
  }
  // ブロックを盤面の上のほうにセットする
  currentX = 5;
  currentY = 0;
}

// 盤面を空にする    //inti()最初に一回だけ発動
function init() {
  for ( var y = 0; y < ROWS; ++y ) {
    board[ y ] = [];
    for ( var x = 0; x < COLS; ++x ) {
      board[ y ][ x ] = 0;    // 盤面を空にするための値生成・色指定
    }
  }
}

// newGameで指定した秒数毎に呼び出される関数。
// 操作ブロックを下の方へ動かし、
// 操作ブロックが着地したら消去処理、ゲームオーバー判定を行う
function tick() {
  // １つ下へ移動する
	if(pause == false){
  if ( valid( 0, 1 )) {   //validはその方向へ移動できるかどうかを返す関数
    ++currentY;
  }
  // もし着地していたら(１つしたにブロックがあったら)
  else {
    freeze();  // 操作ブロックを盤面へ固定する
    clearLines();  // ライン消去処理
    if (lose) {
      // もしゲームオーバなら最初から始める
    	alert("GAME OVER");
      newGame();
      return false;
    }
    // 新しい操作ブロックをセットする
    newShape();
  }
	}
}


// 操作ブロックを盤面にセットする関数
function freeze() {     // 750ﾐﾘ秒毎に呼び出されブロックの動きを決める
  for ( var y = 0; y < 4; ++y ) {
    for ( var x = 0; x < 4; ++x ) {
      if ( current[ y ][ x ] ) {
        board[ y + currentY ][ x + currentX ] = current[ y ][ x ];
      }
    }
  }
}

// 操作ブロックを回す処理
function rotate( current ) {
  var newCurrent = [];
  for ( var y = 0; y < 4; ++y ) {
    newCurrent[ y ] = [];
    for ( var x = 0; x < 4; ++x ) {
      newCurrent[ y ][ x ] = current[ 3 - x ][ y ];
    }
  }
  return newCurrent;
}

// 一行が揃っているか調べ、揃っていたらそれらを消す
function clearLines() {
  for ( var y = ROWS - 1; y >= 0; --y ) {
    var rowFilled = true;
    // 一行が揃っているか調べる
    for ( var x = 0; x < COLS; ++x ) {
      if ( board[ y ][ x ] == 0 ) {
        rowFilled = false;
        break;
      }
    }
    // もし一行揃っていたら, サウンドを鳴らしてそれらを消す。
    if ( rowFilled ) {
      document.getElementById( 'clearsound' ).play();  // 消滅サウンドを鳴らす
      lineScore += 1000;
      document.getElementById("scorePoint").innerHTML = lineScore;
      // その上にあったブロックを一つずつ落としていく
      for ( var yy = y; yy > 0; --yy ) {
        for ( var x = 0; x < COLS; ++x ) {
          board[ yy ][ x ] = board[ yy - 1 ][ x ];
        }
      }
      ++y;  // 一行落としたのでチェック処理を一つ下へ送る
    }
  }
}


// キーボードが押された時に呼び出される関数
function keyPress( key ) {
  switch ( key ) {
  case 'left':
    if ( valid( -1 ) ) {
      --currentX;  // 左に一つずらす
    }
    break;
  case 'right':
    if ( valid( 1 ) ) {
      ++currentX;  // 右に一つずらす
    }
    break;
  case 'down':
    if ( valid( 0, 1 ) ) {
      ++currentY;  // 下に一つずらす
    }
    break;
  case 'rotate':
    // 操作ブロックを回す
    var rotated = rotate( current );
    if ( valid( 0, 0, rotated ) ) {
      current = rotated;  // 回せる場合は回したあとの状態に操作ブロックをセットする
    }
    break;
  case 'pause':
	  if(pause == false){
		  pause = true;
		  document.getElementById( 'bgm' ).pause();
	  } else  {
		  pause = false;
		  document.getElementById( 'bgm' ).play();
	  }
	  break;
  }
}

// 指定された方向に、操作ブロックを動かせるかどうかチェックする
// ゲームオーバー判定もここで行う
function valid( offsetX, offsetY, newCurrent ) {
  offsetX = offsetX || 0;
  offsetY = offsetY || 0;
  offsetX = currentX + offsetX;
  offsetY = currentY + offsetY;
  newCurrent = newCurrent || current;
  for ( var y = 0; y < 4; ++y ) {
    for ( var x = 0; x < 4; ++x ) {
      if ( newCurrent[ y ][ x ] ) {
        if ( typeof board[ y + offsetY ] == 'undefined'
             || typeof board[ y + offsetY ][ x + offsetX ] == 'undefined'
             || board[ y + offsetY ][ x + offsetX ]
             || x + offsetX < 0
             || y + offsetY >= ROWS
             || x + offsetX >= COLS ) {
               if (offsetY == 1 && offsetX-currentX == 0 && offsetY-currentY == 1){
                 console.log('game over');
                 lose = true; // もし操作ブロックが盤面の上にあったらゲームオーバーにする
                 }
               }
               return false;
             }
      }
    }
  }
  return true;
}

function newGame() {
  clearInterval(interval);  // ゲームタイマーをクリア
  init();  // 盤面をまっさらにする
  document.getElementById('bgm').play();
  newShape();  // 新しい
  lose = false;
  interval = setInterval( tick, 750 );  // 750ミリ秒ごとにtickという関数を呼び出す
}
newGame();  // ゲームを開始する
